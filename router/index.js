'use strict'

/*
 * Dependencies
 */
const path = require('path')
const course = require('course')
const st = require('st')
const jsonBody = require('body/json')
const animateClient = require('animate-client')
const helper = require('../helper')

/*
 * Local config
 */
const router = course()

const mount = st({
	path : animateClient,
	index : 'index.html',
	passthrough : true
})

/*
 * Helpers (middlewares)
 */
function onRequest (req, res) {
	if (req.url.startsWith('/socket.io'))
		return

	mount(req, res, onMount)
}

function fail (err, res) {
	res.statusCode = 500
	res.setHeader('Content-Type', 'text/plain')
	res.end(err.message)
}

function onMount (err) {
	if (err)
		return fail(err, res)

	router(req, res, onRouter)
}

function onRouter (err) {
	if (err)
			return fail(err, res)

		res.statusCode = 404
		res.end(`404 Not Found: ${req.url}`)
}

/*
 * Exporting
 */
module.exports = onRequest