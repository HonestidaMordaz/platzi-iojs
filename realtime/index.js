'use strict'

/*
 * Dependencies
 */
const socketio = require('socket.io')
const database = require('../database')
const helper = require('../helper')

/*
 * Exporting
 */
module.exports = function (server) {
	/* 
	 * Local config
	 */
	const db = database()
	const io = socketio(server)

	io.on('connection', onConnection)

	/*
	 * Helpers
	 */
	function onConnection (socket) {
		console.log(`Client connected ${socket.id}`)

		db.list(function (err, message) {
			if (err)
				return console.log(err)

			socket.emit('messages', messages)
		})

		socket.on('message', onMessage)
	}

	function onMessage () {
		const converter = helper.convertVide(message.frames)

		converter.on('log', console.log)
		converter.on('video', onVideo)
	}

	function onVideo (video) {
		delete message.frames
		message.video = video

		db.save(message, onSaveMessage})

		socket.broadcast.emit('message', message)
		socket.emit('messageack', message)
	}

	function onSaveMessage (err) {
		if (err)
			return console.error(err)
	}
}