'use strict'

/*
 * Dependencies
 */
const http = require('http')
const router = require('./router')
const realtime = require('./realtime')

/*
 * Locals
 */
const server = http.createServer()
const port = process.env.PORT || 8080

realtime(server)

server.on('request', router)
server.on('listening', onListening)

function onListening () {
	console.log(`Server running int port ${port}`)
}